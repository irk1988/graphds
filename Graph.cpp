/* 
 * Program to implement the shortest path for a given undirected graph G = (V,E)
 * Author : Philip Karunakaran, Immanuel Rajkumar
 */

#include<iostream>
#include<ctime>
#include<vector>
#include<list>
#include<queue>
#include<climits>

using namespace std;

int INF;

class Vertex;

/*
 * Edge -  Represents an edge in the graph which are connected by two 'Vertex's
 */
class Edge {

	public:

	Vertex * from;
	Vertex * to;
	int weight;

	Edge ( Vertex * u, Vertex * v, int w ) {
		this->from = u;
		this->to = v;
		this->weight = w;
	}

	Vertex * otherEnd(Vertex * u ) {
		if( from == u ) return to;
		else return from;
	}

	bool operator < ( const Edge * e ) const {
		return this->weight < e->weight;
	}
};

/*
 * Vertex - Represents a vertex in the graph 
 */
class Vertex {

	public:

	int name;
	bool seen;
	bool inS;
	Vertex * parent;
	int distance;
	int spts;
	list<Edge *> adj;

	Vertex (int n) {
		this->name = n;
		this->seen = false;
		this->inS = false;
		this->parent = NULL;
		this->distance = 0;
		this->spts = 0;
	}
};

/* Vertex comparator */
struct comparator {
	bool operator () ( const Vertex * u, const Vertex * v) {
		return u->distance > v->distance;
	}
};

/*
 * Graph representation of G=(V,E)
 */
class Graph {
	
	public:
	


	vector<Vertex *> V;
	int n;

	Graph (int s) {
		this->n = s;
		V.resize(s+1);
		for( int i=0; i<=s; i++ ) 
			V[i] = new Vertex(i);
	}

	void addEdge( int u, int v, int w ) {
		Edge * e  = new Edge( V[u], V[v], w );
		V[u]->adj.push_back(e);
		V[v]->adj.push_back(e);
	}

	int DFS(Vertex * u) {
		cout<<"\n Visiting -> "<<u->name;
		u->seen = true;
		
		list<Edge *>::iterator i, BEGIN = u->adj.begin(), END = u->adj.end();
		for( i=BEGIN; i!=END; ++i ) {
			//cout<<"\nChecking now : ( "<<(*i)->from->name<<","<<(*i)->to->name<<" )";
			if( (*i)->from == u ) {
				if( !(*i)->to->seen ) {
					DFS( (*i)->to );
				}
			}
		}
		return 0;
	}

	int BFS(Vertex * u) {
		queue<Vertex *> q;
		q.push(u);
		u->seen = true;	
		Vertex * x;
		while( !q.empty() ) {
			x = q.front();
			q.pop();
			cout<<"\n Visiting : "<<x->name;

			list<Edge *>::iterator i, BEGIN = x->adj.begin(), END = x->adj.end();
			for( i=BEGIN; i!=END; ++i ) {
				Vertex * v = (*i)->otherEnd(x);
				//cout<<"\nChecking now : ( "<<u->name<<","<<v->name<<" )";
				if( !v->seen ) {
					v->seen = true;
					q.push(v);
				}
			}	
		}
		delete(x);
		return 0;
	}

	int Prims( Vertex * src ) {
		priority_queue<Vertex *, vector<Vertex *>, comparator > PQ;
		
		//Set the distance as inf , can be set in the constructor itself if no sub algorithms are not used
		for( int i=1; i<= this->n; i++ ) {
			V[i]->distance = INF;
		}
		src->distance = 0;
		src->inS = true;

		for( int i=1; i<= this->n; i++ ) {
			PQ.push(V[i]);
		}

		Vertex * u, * v;
		while( !PQ.empty() ) {
			u = PQ.top();	
			PQ.pop();
			u->inS = true;
			cout<<"\n From PQ : "<<u->name<<" Distance :"<<u->distance;

			list<Edge *>::iterator i, BEGIN = u->adj.begin(), END =  u->adj.end();
			for( i = BEGIN; i != END; i++ ) {
				if( (*i)->from->name == u->name ) {
					v = (*i)->otherEnd(u);
					cout<<"\n Edge now : "<<u->name<<"-->"<<v->name<<" weight :"<<(*i)->weight;
					if( !v->inS && (*i)->weight < v->distance ) {
						v->parent = u;
						v->distance = (*i)->weight;
						//Instead of the decrease key, the below make_heap is used.
						make_heap(const_cast<Vertex**>(&PQ.top()),const_cast<Vertex**>(&PQ.top())+ PQ.size(),comparator());
					}
				}
			}
		}
	
		cout<<"\n MST : \n";	
		for( int i=1; i<= this->n; i++ ) {
			cout<<"\n\tNode : "<<V[i]->name;
			if( V[i]->parent != NULL ) cout<<" Parent :"<<V[i]->parent->name;
		}

		delete(u);
		delete(v);

		return 0;
	}

	int BellmanFord( Vertex * src ) {
		
		for( int i=1; i<= this->n; i++ ) {
			V[i]->distance = INF;
		}

		src->distance = 0;
		cout<<"\n Here \n";
		Vertex *v, *u;
		// from 1 to |v| - 1
		for( int k=1; k<= this->n; k++ ) {
			//This outer iteration parameter is not used anywhere, just for the count of the edges
			u = V[k];
			cout<<"\n Processing Node : "<<u->name<<endl;
			list<Edge *>::iterator i, BEGIN = u->adj.begin(), END =  u->adj.end();
			for( i = BEGIN; i != END; i++ ) {
				v = (*i)->otherEnd(u);
				if( (*i)->from->name == u->name ) {
					if( v->distance > u->distance + (*i)->weight ) {
						v->distance = u->distance + (*i)->weight;
						v->spts++;
					}
					else if( v->distance == u->distance + (*i)->weight )
						v->spts = u->spts+1;// Needs modification here
				}
			}
		}

		cout<<"\n All pairs shortest path : \n";
		for( int i=1; i<= this->n; i++ ) {
			cout<<"\n Vertex :"<<V[i]->name<<" Distance : "<<V[i]->distance<<" Paths : "<<V[i]->spts;
		}
		
	}

	int Dijkstra( Vertex * src ) {
		//Needs modification
		priority_queue<Vertex *, vector<Vertex *>, comparator > PQ;
		
		//Set the distance as inf , can be set in the constructor itself if no sub algorithms are not used
		for( int i=1; i<= this->n; i++ ) {
			V[i]->distance = INF;
		}
		src->distance = 0;
		src->inS = true;

		for( int i=1; i<= this->n; i++ ) {
			PQ.push(V[i]);
		}

		Vertex * u, * v;
		while( !PQ.empty() ) {
			u = PQ.top();	
			PQ.pop();
			u->inS = true;
			cout<<"\n From PQ : "<<u->name<<" Distance :"<<u->distance;

			list<Edge *>::iterator i, BEGIN = u->adj.begin(), END =  u->adj.end();
			for( i = BEGIN; i != END; i++ ) {
				if( (*i)->from->name == u->name || (*i)->to->name == u->name ) {
					v = (*i)->otherEnd(u);
					cout<<"\n Edge now : "<<u->name<<"-->"<<v->name<<" weight :"<<(*i)->weight;
					if( !v->inS && (*i)->weight < v->distance ) {
						v->parent = u;
						v->distance = u->distance + (*i)->weight;
						//Instead of the decrease key, the below make_heap is used.
						make_heap(const_cast<Vertex**>(&PQ.top()),const_cast<Vertex**>(&PQ.top())+ PQ.size(),comparator());
					}
				}
			}
		}
	
		cout<<"\n MST : \n";	
		for( int i=1; i<= this->n; i++ ) {
			cout<<"\n\tNode : "<<V[i]->name;
			if( V[i]->parent != NULL ) cout<<" Parent :"<<V[i]->parent->name;
			cout<<" Distance : "<<V[i]->distance;
		}

		return 0;
	}
};

/*
 * Graph driver program
 */
int driver( Graph& g ) {
//	cout<<"\nCalling the driver program for the graph \n";
//	Vertex * src = g.V[1];
//	cout<<"\n\n DFS : \n\n";
//	g.DFS(src);
//	cout<<"\n\n BFS : \n\n";
//	g.BFS(src);
//	cout<<"\n\n Dijikstra : \n\n";
//	g.Dijkstra(src);
//	cout<<"\n\n Prims : \n\n";
//	g.Prims(src);
	cout<<"\n\n Bellman ford : \n\n";
	g.BellmanFord(src);

	return 0;	
}	


int main() {

	INF = INT_MAX;
	clock_t start, end;
	double dur = 0;
	/* Starting the clock */
	start = clock();

	int en, vn;
	int src, t;

	cin>>vn>>en>>src>>t;
	
	Graph g(vn);

	int u, v, w;	
	for( int i=0; i<en; i++ ) {
		cin>>u>>v>>w;
		g.addEdge(u,v,w);
	}
	
	//cout<<"\nSize of the current graph in consideration : "<<G.n<<"  "<<G.V.size()<<endl;
	/* Ending the clock */
	driver(g);
	
	end = clock();

	dur = (double)(end - start)/CLOCKS_PER_SEC*1000;
	cout<<"\nRuntime: "<<dur<<" msec"<<endl;

	return 0;
}
